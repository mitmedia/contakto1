Contakco System
==============

Installation Steps
------

BE

* [Create a virtual environment](http://desarrolloweblibre.com/por-que-usar-virtualenv/) and activate it (`$ . bin/activate` or `$ source bin/activate`)
* Clone project
* Checkout to **dev** branch
* Create your branch (**topics/feature**)
* Install python packages (`$ pip install -r requirements.txt`)
* Run server (`$ python manage.py runserver `)

### NOTES:
* In case of using Osx you might need to install Xcode
* To improve speed when generating sprites, install oily (gem install oily_png) (http://compass-style.org/help/tutorials/spriting/)
